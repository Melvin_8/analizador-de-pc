# ;)
# ! {<J>}
# ! author: Julio C moreira
# ! made between 02/22/22 to //22
# TODO: menu prin -> espec pc > mem ram > tot de mem usada > % do cpu (informar info do cpu)
 
# * BIBLIOTECAS {
from psutil import cpu_count,cpu_freq,cpu_percent,virtual_memory
import prettytable
from time import sleep
# * }  


# * MENU PRIN {
sleep(0.5)
print('\033[32m#','-'*12,'#\n','  pc analyzer','\n#','-'*12,'#',end='\n\n')
cpu_per = cpu_percent(0.6)
cpu_cont = cpu_count()
cpu_freq = str(cpu_freq()).replace('scpufreq(','').replace(')','')
ram = virtual_memory()
ram_por = ram[2]
ram_tot = ram[0]
ram_ava = ram[1]
x = prettytable.PrettyTable(["cpu","ram"])
x.padding_width = 1
x.add_row([f'percent: {cpu_per}',f'percent: {ram_por}'])
x.add_row([f'cores: {cpu_cont}',f'total: {ram_tot}'])
x.add_row([f'frequence: {cpu_freq}',f'available: {ram_ava}'])
print(x)
# * }
